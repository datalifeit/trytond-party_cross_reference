# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval

__all__ = ['PartyCrossReference']


class PartyCrossReference(ModelSQL, ModelView):
    """Party Cross reference"""
    __name__ = 'party.cross_reference'

    party = fields.Many2One('party.party', 'Party', required=True,
        select=True, ondelete='RESTRICT')
    dest_party = fields.Many2One('party.party', 'Destination party',
        required=True, select=True, ondelete='RESTRICT')
    address = fields.Many2One('party.address', 'Address',
        domain=[('party', '=', Eval('dest_party'))],
        depends=['dest_party'])
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
