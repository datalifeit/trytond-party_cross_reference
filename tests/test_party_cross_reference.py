# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
import unittest
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.tests.test_tryton import suite as test_suite
from trytond.pool import Pool


class PartyCrossReferenceTestCase(ModuleTestCase):
    """Test Party Cross Reference module"""
    module = 'party_cross_reference'

    @with_transaction()
    def test0010create_cross_reference(self):
        pool = Pool()
        Party = pool.get('party.party')
        Reference = pool.get('party.cross_reference')

        pty1, pty2 = Party.create([{
            'name': 'Customer 1'}, {
            'name': 'Customer 2'}])
        cr1, = Reference.create([{
            'party': pty1.id,
            'dest_party': pty2.id,
            'code': '001',
            'name': 'P001'}])


def suite():
    suite = test_suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(
            PartyCrossReferenceTestCase))
    return suite
