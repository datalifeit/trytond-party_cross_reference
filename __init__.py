# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .party import PartyCrossReference


def register():
    Pool.register(
        PartyCrossReference,
        module='party_cross_reference', type_='model')
